Copyright &copy; 2014-2019 Kirk Rader. All rights reserved.

![Kirk][logo]

# Examples

```mermaid
graph TB
    start(( ))
    inception[Inception / Business Analysis]
    execution[Execution / Development]
    verification[Verification / QA]
    operation[Operation / Tech Support]
    start-- request feature -->inception
    inception-- reject -->start
    inception-- analyze -->inception
    inception-- approve -->execution
    execution-- design / implement -->execution
    execution-- request change -->inception
    execution-- release -->verification
    verification-- report bug -->execution
    verification-- test -->verification
    verification-- request change -->inception
    verification-- accept -->operation
    operation-- monitor -->operation
    operation-- report bug -->execution
    operation-- request change -->inception
    operation-- anayltics -->inception
```

- <http://www.rader.us>

[logo]: https://gitlab.com/kirkrader/kirkrader.gitlab.io/raw/master/static/logo.png
