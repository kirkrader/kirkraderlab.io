---
title: Kirk Rader
type: docs
---

# Kirk Rader

**Software architecture, design and implementation**

---

{{< mermaid >}}
graph TB
    start(( ))
    inception[Inception]
    execution[Execution]
    verification[Verification]
    operation[Operation]
    start-- request feature -->inception
    inception-- reject -->start
    inception-- analyze -->inception
    inception-- approve -->execution
    execution-- design / implement -->execution
    execution-- request change -->inception
    execution-- release -->verification
    verification-- report bug -->execution
    verification-- test -->verification
    verification-- request change -->inception
    verification-- accept -->operation
    operation-- monitor -->operation
    operation-- report bug -->execution
    operation-- request change -->inception
    operation-- anayltics -->inception
{{< /mermaid >}}

---

## Highlights

- [Configuring a Raspberry Pi][rpi-config]
- [Using Node-RED][node-red]
- [Symbolic Logic][symbolic-logic]
- [Scheme][]

## This Site

- <http://www.rader.us>
- <https://kirkrader.gitlab.io>
- (Source <https://gitlab.com/kirkrader/kirkrader.gitlab.io>)

## More

- <http://www.cdbaby.com/Artist/KirkRader>
- <https://gitlab.com/parasaurolophus>

[rpi-config]: {{< relref "docs/rpi-config/" >}}
[node-red]: https://gitlab.com/parasaurolophus/cheznous
[symbolic-logic]: {{< relref "docs/symbolic-logic/" >}}
[scheme]: {{< relref "docs/scheme/" >}}
