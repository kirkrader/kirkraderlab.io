---
title: "Exfat"
date: 2019-09-11T06:19:06-07:00
anchor: "exfat"
weight: 60
---

# Exfat Fuse

Install *exfat* file system support via [exfat-fuse][].

## Script

[config-exfat.sh][]

{{< code file="/static/rpi-config/config-exfat.sh" >}}

## Usage

```bash
./config-exfat.sh
```

## Additional Info

See [Automount an Exfat Drive][automount-page] for information on
mounting a given exfat drive automatically.

[config-exfat.sh]: /rpi-config/config-exfat.sh
[exfat-fuse]: https://packages.debian.org/buster/exfat-fuse
[automount-page]: {{< relref "automount-exfat.md" >}}
