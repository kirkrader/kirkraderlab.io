---
title: "Automount Exfat"
date: 2019-09-11T06:19:07-07:00
anchor: "automount-exfat"
weight: 70
---

# Automount an Exfat Drive

Mount an *exfat* drive automatically using the UUID displayed by `blkid`.

## Script

[automount-exfat.sh][]

{{< code file="/static/rpi-config/automount-exfat.sh" >}}

## Usage

```bash
./automount-exfat.sh -d DIRECTORY -u UUID
```

## Additional Information

1. Invoke `blkid` to see your drive's UUID, e.g.:

  ```bash
  blkid
  /dev/sda1: LABEL="boot" UUID="DDAB-3A15" TYPE="vfat" PARTUUID="fb2bc19d-01"
  /dev/sda2: LABEL="rootfs" UUID="5fa1ec37-3719-4b25-be14-1f7d29135a13" TYPE="ext4" PARTUUID="fb2bc19d-02"
  /dev/sdb1: UUID="5EA9-AFBB" TYPE="exfat" PTTYPE="dos" PARTLABEL="Microsoft basic data" PARTUUID="6b400c2e-5e2c-48b8-a05e-61ff41e48a66"
  ```

  meaning that UUID is 5EA9-AFBB in my case

2. Mount drive 5EA9-AFBB at */data* each time the Pi boots:

  ```bash
  ./rpi-automation-exfat -d /data -u 5EA9-AFBB
  ```

> You must the actual UUID reported by `blkid` on your system, not the
> example UUID shown above.

[automount-exfat.sh]: /rpi-config/automount-exfat.sh
