---
title: "Emacs"
date: 2019-09-11T06:19:07-07:00
anchor: "emacs"
weight: 130
---

# Emacs

Install and configure [emacs][], [markdown-mode][] and [pandoc][].

## Script

[config-emacs.sh][]

{{< code file="/static/rpi-config/config-emacs.sh" >}}

## Usage

```bash
./config-emacs.sh
```

[config-emacs.sh]: /rpi-config/config-emacs.sh
[emacs]: https://www.gnu.org/software/emacs/
[markdown-mode]: https://github.com/jrblevin/markdown-mode
[pandoc]: https://pandoc.org/
