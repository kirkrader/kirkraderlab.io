---
title: "Hugo"
weight: 160
# bookFlatSection: false
# bookShowToC: true
---

# Hugo

Install and configure [Hugo][]

## Script

[config-hugo.sh][]

{{< code file="/static/rpi-config/config-hugo.sh" >}}

## Usage

```bash
./config-hugo.sh
```

# Additional Information

You must first install [snapd][]. See [Snap][snap-page] for details.

[config-hugo.sh]: /rpi-config/config-hugo.sh
[snapd]: https://snapcraft.io/
[hugo]: https://gohugo.io/
[snap-page]: {{< relref "docs/rpi-config/snap/_index.md" >}}
