---
title: "Mosquitto"
date: 2019-09-11T06:19:07-07:00
anchor: "mosquitto"
weight: 50
---

# Mosquitto

Install [mosquitto][] MQTT broker.

## Script

[config-mosquitto.sh][]

{{< code file="/static/rpi-config/config-mosquitto.sh" >}}

## Usage

```bash
./config-mosquitto.sh
```

## Additional Information

- Configure your router to forward port 1883 to your Pi
- Set up [mosquitto][] using [config-mosquitto.sh][]

<a id="secure_mqtt"></a>

### Securing Your MQTT Broker

If you wish to use a MQTT dashboard app on your laptop or mobile
device for remote access to your home automation gear then you should
enable password authentication and encryption for your MQTT broker.

- Follow the instructions at
  <https://mosquitto.org/man/mosquitto_passwd-1.html> to create a
  *passwd* file containing your desired credentials, e.g.

  ```
  sudo mosquitto_passwd -c /etc/mosquitto/passwd jdoe
  ```

- Edit */etc/mosquitto/mosquitto.conf* to specify the *passwd* file
  you created, e.g.

  ```bash
  sudo nano /etc/mosquitto/mosquitto.conf
  ```

  and insert:

  ```
  allow_anonymous false
  password_file /etc/mosquitto/passwd
  ```

- To enable TLS encryption, use the `capath`, `certfile` and `keyfile`
  options to reference your *letsencrypt* certificate and key files
  installed via [certbot][certbot-page], e.g. insert something like
  the following in */etc/mosquitto/mosquitto.conf*:

  ```
  capath /etc/letsencrypt/live/yourdomain.duckdns.org/
  certfile /etc/letsencrypt/live/yourdomain.duckdns.org/fullchain.pem
  keyfile /etc/letsencrypt/live/yourdomain.duckdns.org/privkey.pem
  ```

<a id="permissions"></a>

Note that for the latter, you need to make sure that the user account
that the *mosquitto* service runs as has read permissions to the
relevant *letsencrypt* files. Depending on exactly how you set all of
this up, you may either have to loosen the security permissions on the
files and directories under */etc/letsencrypt/live/* or use a
post-renewal script to make copies in some other location. See
<https://certbot.eff.org/docs/using.html> and
<https://mosquitto.org/man/mosquitto-conf-5.html> for details.

[config-mosquitto.sh]: /rpi-config/config-mosquitto.sh
[mosquitto]: http://mosquitto.org/
[certbot-page]: {{< relref "docs/rpi-config/apache/certbot.md" >}}
