---
title: "Docker"
date: 2019-09-11T06:19:07-07:00
anchor: "docker"
weight: 120
---

# Docker

Install and configure [Docker][] container engine.

## Script

[config-docker.sh][]

{{< code file="/static/rpi-config/config-docker.sh" >}}

## Usage

```bash
./config-docker.sh
```

[config-docker.sh]: /rpi-config/config-docker.sh
[Docker]: https://www.docker.com/
