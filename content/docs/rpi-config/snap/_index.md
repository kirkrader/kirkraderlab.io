---
title: "Snap"
weight: 150
# bookFlatSection: false
# bookShowToC: true
---

# Snap

Install and configure [snapd][]

## Script

[config-snap.sh][]

{{< code file="/static/rpi-config/config-snap.sh" >}}

## Usage

```bash
./config-snap.sh
```

# Additional Information

You must reboot your machine after installing `snapd` before
invocations of `snap` will succeed. After that, you may install the
Snap Store using:

```bash
sudo snap install snap-store
```

[config-snap.sh]: /rpi-config/config-snap.sh
[snapd]: https://snapcraft.io/
