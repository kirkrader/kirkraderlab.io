---
title: "Duck DNS"
date: 2019-09-11T06:19:06-07:00
anchor: "duckdns"
weight: 20
---

# Duck DNS

Set up a dynamic DNS cron job using domain name and access token from
[Duck DNS][].

## Script

[config-duckdns.sh][]

{{< code file="/static/rpi-config/config-duckdns.sh" >}}

## Usage

```bash
./config-duckdns.sh -d DOMAIN -t TOKEN
```

## Additional Information

1. Create an account at <http://www.duckdns.org> if you do not already
   have one.

2. Follow the instructions there to create the domain you want to use
   for your Pi if you have not already done so.

3. Use the FQDN (e.g. `mypi.duckdns.org`) and access token for the
   domain you created in step 2 when invoking [config-duckdns.sh][].

In order for the configuration performed by [config-duckdns.sh][] to
be effective, you must at minimum also configure your router to
forward whatever ports you want exposed to the Internet to your Pi and
set up the corresponding services such as [Apache][apache-page],
[Mosquitto][mosquitto-page] and so on.

[config-duckdns.sh]: /rpi-config/config-duckdns.sh
[Duck DNS]: http://www.duckdns.org
[apache-page]: {{< relref "docs/rpi-config/apache/_index.md" >}}
[mosquitto-page]: {{< relref "docs/rpi-config/mosquitto/_index.md" >}}
