---
title: "Configuring a Raspberry Pi"
weight: 1
# bookFlatSection: false
# bookShowToC: true
---

# Configuring a Raspberry Pi

## Supported Hardware

- Raspberry Pi [3B+][] or [4B][] running Raspbian [buster][]
- [OpenZWave][] compatible Z-Wave controller (e.g. [Aeotec Z-Stick Gen5][])

## Supported Features

{{< mermaid >}}
graph TB

    subgraph Home Network

        hubitat[Hubitat Elevation]
        bridge[Hue Bridge]

        subgraph Raspberry Pi
            nodered[Node-RED]
            zstick[Aeotec Z-Stick]
            mosquitto[Mosquitto]

            nodered == OpenZWave === zstick
            nodered == MQTT === mosquitto
        end

        more[more devices]

        mosquitto == MQTT === hubitat
        nodered == "Hue API" === bridge
        zstick -. Z-Wave .- zwave
        bridge -. Zigbee .- hue
        hubitat -.- more
        hue[Hue Devices]
        zwave[Z-Wave Devices]

    end

    subgraph Phone
        location[Owntracks]
        dashboard[Dashboard]
    end

    location == MQTT / TLS === mosquitto
    dashboard == HTTPS === nodered

    classDef feature fill:#ffaaff,stroke:#000000,stroke-width:1px
    class nodered,mosquitto,zstick,hubitat feature
{{< /mermaid >}}

- Secure remote access over the Internet
  - Public DNS name
  - Encrypted communications
  - Password protected
- Web server
- [MQTT broker][mosquitto-page]
- Home automation server (i.e. [Node-RED][] which is one of Raspberry
  Pi's recommended software applications, i.e. installed by default in
  the "full" version of [Raspbian][])
- [Z-Wave][] controller
- [DLNA][] media server
- SMB file server
  - *exfat* filesystem support

## Configuration Scripts

| Script                  | Usage                                                | Description                                                                                                   |
|:------------------------|:-----------------------------------------------------|:--------------------------------------------------------------------------------------------------------------|
| [config-duckdns.sh][]   | `./config-duckdns.sh -d DOMAIN -t TOKEN`             | Set up dynamic DNS using domain name and access token from [Duck DNS][]                                       |
| [config-apache2.sh][]   | `./config-apache2.sh`                                | Install [Apache][] server                                                                                     |
| [config-certbot.sh][]   | `./config-certbot.sh`                                | Install [certbot][] certificate into Apache web server                                                        |
| [config-mosquitto.sh][] | `./config-mosquitto.sh`                              | Install [mosquitto][] MQTT broker                                                                             |
| [config-openzwave.sh][] | `./config-openzwave.sh`                              | Install the [OpenZWave][] driver library                                                                      |
| [zstick-configtty.sh][] | `./zstick-configtty.sh -v VENDOR -p PRODUCT -n NAME` | Configure *udev* to give a predictable name to a USB Z-Wave controller using information obtained via `lsusb` |
| [config-dlna.sh][]      | `./config-dlna.sh`                                   | Install and configure [minidlna][] media server                                                               |
| [config-samba.sh][]     | `./config-samba.sh -u USER -p PASSWORD`              | Install and configure [samba][] file server                                                                   |
| [config-exfat.sh][]     | `./config-exfat.sh`                                  | Install *exfat* file system support via [exfat-fuse][]                                                        |
| [automount-exfat.sh][]  | `./automount-exfat.sh -d DIRECTORY -u UUID`          | Mount an *exfat* drive automatically using the UUID displayed by `blkid`                                      |
| [config-docker.sh][]    | `./config-docker.sh`                                 | Install and configure [Docker][] container engine                                                             |
| [config-emacs.sh][]     | `./config-emacs.sh`                                  | Install and configure [emacs][], [markdown-mode][] and [pandoc][]                                             |

> Note that the same techniques illustrated by these scripts can be
> extended to support other dedicated home automation services such as
> [OpenHAB][], [Home Assistant][] etc. This is the main purpose of
> installing [Docker][]

## Additional Information

Minimum requirements for a home automation (or any other kind of)
server with out-of-home access are:

1. Enable remote access over the Internet using [dynamic
   DNS][duckdns-page]
2. Secure access using TLS ("https")
   - See [Apache][apache-page] to install the [Apache][] web server software
   - See [certbot][certbot-page] to obtain a free, automatically renewing
     certificate and install it

Additional features for common home automation scenarios:

3. See [Mosquitto][mosquitto-page] to install a [MQTT][] broker
4. See [Node-RED][nodered-page] for securing your [Node-RED][]
   instance for remote access of the Internet
5. See [OpenZWave][openzwave-page] to use your Pi as a [Z-Wave][] controller
   hub

 > Obviously, a [Z-Wave][] controller is only necessary if you want to
 > control [Z-Wave][] devices. Similarly, the [OpenZWave][] library is
 > only required when using your Pi as a native hub by way of, for
 > example, [node-red-contrib-openzwave-am][] or via native support
 > for [OpenZWave][] in a product like [OpenHAB][]. If you don't care
 > about Z-Wave or have a dedicated [Z-Wave][] hub like [Hubitat
 > Elevation][hubitat] or [Samsung SmartThings][smartthings] then
 > there is no point to having a [Z-Wave][] controller device attached
 > to your Pi but you may still find [Node-RED][] useful as a better
 > automation engine than those built into stand-alone hubs. See
 > <https://gitlab.com/parasaurolophus/cheznous> for information on such
 > scenarios.

[config-duckdns.sh]: /rpi-config/config-duckdns.sh
[config-apache2.sh]: /rpi-config/config-apache2.sh
[config-certbot.sh]: /rpi-config/config-certbot.sh
[config-mosquitto.sh]: /rpi-config/config-mosquitto.sh
[config-openzwave.sh]: /rpi-config/config-openzwave.sh
[zstick-configtty.sh]: /rpi-config/zstick-configtty.sh
[config-dlna.sh]: /rpi-config/config-dlna.sh
[config-samba.sh]: /rpi-config/config-samba.sh
[config-exfat.sh]: /rpi-config/config-exfat.sh
[config-docker.sh]: /rpi-config/config-docker.sh
[automount-exfat.sh]: /rpi-config/automount-exfat.sh
[config-emacs.sh]: /rpi-config/config-emacs.sh

[Duck DNS]: http://www.duckdns.org
[Apache]: https://httpd.apache.org
[certbot]: https://certbot.eff.org/
[mosquitto]: http://mosquitto.org/
[3B+]: https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/
[4b]: https://www.raspberrypi.org/products/raspberry-pi-4-model-b/
[buster]: https://www.raspberrypi.org/downloads/
[OpenZWave]: http://www.openzwave.com/
[Aeotec Z-Stick Gen5]: https://aeotec.com/z-wave-usb-stick/
[Node-RED]: https://nodered.org/
[minidlna]: https://wiki.debian.org/minidlna
[samba]: https://www.samba.org
[exfat-fuse]: https://packages.debian.org/buster/exfat-fuse
[Docker]: https://www.docker.com/
[OpenHAB]: https://www.openhab.org/
[Home Assistant]: https://www.home-assistant.io/
[node-red-contrib-openzwave-am]: https://flows.nodered.org/node/node-red-contrib-openzwave-am
[emacs]: https://www.gnu.org/software/emacs/
[markdown-mode]: https://github.com/jrblevin/markdown-mode
[pandoc]: https://pandoc.org/
[mqtt]: http://mqtt.org/
[z-wave]: https://z-wavealliance.org/
[dlna]: https://www.dlna.org/
[raspbian]: https://www.raspbian.org/
[hubitat]: https://hubitat.com/
[smartthings]: https://www.smartthings.com/

[duckdns-page]: {{< relref "docs/rpi-config/duckdns/_index.md" >}}
[apache-page]: {{< relref "docs/rpi-config/apache/_index.md" >}}
[certbot-page]: {{< relref "docs/rpi-config/apache/certbot.md" >}}
[mosquitto-page]: {{< relref "docs/rpi-config/mosquitto/_index.md" >}}
[openzwave-page]: {{< relref "docs/rpi-config/openzwave/_index.md" >}}
[exfat-page]: {{< relref "docs/rpi-config/exfat/_index.md" >}}
[nodered-page]: {{< relref "docs/rpi-config/nodered/_index.md" >}}
