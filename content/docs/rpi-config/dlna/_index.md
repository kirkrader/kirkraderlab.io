---
title: "DLNA"
date: 2019-09-11T06:19:07-07:00
anchor: "dlna"
weight: 110
---

# DLNA

Install and configure [minidlna][] media server.

## Script

[config-dlna.sh][]

{{< code file="/static/rpi-config/config-dlna.sh" >}}

## Usage

```bash
./config-dlna.sh
```

## Additional Information

This script not only installs [minidlna][] but also creates the
following directories and configures them as the locations from which
to serve their respective types of content:

- */shared/Music*
- */shared/Picture*
- */shared/Video*


[config-dlna.sh]: /rpi-config/config-dlna.sh
[minidlna]: https://wiki.debian.org/minidlna
