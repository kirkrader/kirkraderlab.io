---
title: "OpenZWave"
date: 2019-09-11T06:19:06-07:00
anchor: "openzwave"
weight: 90
---

# OpenZWave

Install the [OpenZWave][] driver library.

## Script

[config-openzwave.sh][]

{{< code file="/static/rpi-config/config-openzwave.sh" >}}

## Usage

```bash
./config-openzwave.sh
```

## Additional Information

1. Use `lsusb` to display your USB device details before inserting
   your [Z-Wave][] controller device
2. Plug a USB based [Z-Wave][] controller into your Pi
3. Use `lsusb` again and see the details for the new device
4. Provide a reliable, human-readable name for your [Z-Wave][]
   controller using [zstick-configtty.sh][] using the information
   obtained via `lsusb`

[config-openzwave.sh]: /rpi-config/config-openzwave.sh
[zstick-configtty.sh]: /rpi-config/zstick-configtty.sh
[OpenZWave]: http://www.openzwave.com/
[z-wave]: https://z-wavealliance.org/
