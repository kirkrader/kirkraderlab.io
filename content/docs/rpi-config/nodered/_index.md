---
title: "Node-RED"
date: 2019-09-11T20:49:42-07:00
weight: 140
anchor: "nodered"
---

# Node-RED

The version of [Node-RED][] that can be installed easily through
[Raspbian's][raspbian] "recommended software" mechanism is quite out of date.
You should follow the instructions for [Alternative
Installations](#alternatives), below, even for [Raspbian][].

To access the [Node-RED][] web-based user interface from outside your home
requires exposing it over the Internet:

- Configure your router to forward port 1880 to your Pi

- Configure your [Node-RED][] instance to require authentication and
  use encryption as described [below](#secure)

  <a id="alternatives"></a>

## Alternative Installations

As noted above, [Node-RED][] is installed by default as one of the
"recommended software" packages in the "full" version of [Raspbian][]
or can be installed through [Raspbian's][raspbian] software
configuration utility if you chose to install a version without such
applications pre-installed.

If you are using a different operating system, there are many ways to
install it depending on the particular OS, version of [node.js][],
version of [Node-RED][] etc. you want to use. See
<https://nodered.org/docs/getting-started/local> for details. How you
install [Node-RED][] does have some consequences for day-to-day
operation. For example, if you install it using the "official" script
for [Debian][] based distros then you will have to run the following manually
to run [Node-RED][] automatically each time you reboot your machine:

```bash
sudo systemctl enable nodered
```

Follow the instructions at
<https://nodered.org/docs/faq/starting-node-red-on-boot> to configure
it to run as a service if you installed [Node-RED][] in some other
way.

<a id="secure"></a>

## Securing Node-RED

As with your [MQTT][mosquitto-page] broker, it would be a mistake to
expose your Node-RED server to the Internet without securing it. Doing
so requires customization of Node-RED's *settings.js* file.

The location of your Node-RED *settings.js* file will vary depending
on whether or not you use the default installation via Raspberry Pi's
"recommended software" configuration or choose to install a different
version manually. Usually, it will be in *~/.node-red/settings.js*.

To secure your [Node-RED][] instance:

- As noted in a comment in the default *settings.js* configuration,
  you must uncomment the line requiring the `fs` module:

  ```
  // The `https` setting requires the `fs` module. Uncomment the following
  // to make it available:
  var fs = require("fs");
  ```

- You must also uncomment and adjust the `adminAuth` property:

  ```
  // Securing Node-RED
  // -----------------
  // To password protect the Node-RED editor and admin API, the following
  // property can be used. See http://nodered.org/docs/security.html for details.
  adminAuth: {
    type: "credentials",
    users: [{
        username: "...user name...",
        password: "...hashed password...",
        permissions: "*"
    }]
  },
  ```

- Also, enable the use of TLS ("https") using the `fs` module:

  ```
  // The following property can be used to enable HTTPS
  // See http://nodejs.org/api/https.html#https_https_createserver_options_requestlistener
  // for details on its contents.
  // See the comment at the top of this file on how to load the `fs` module used by
  // this setting.
  //
  https: {
    key: fs.readFileSync('/etc/letsencrypt/live/yourdomain.duckdns.org/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/yourdomain.duckdns.org/fullchain.pem')
  },

  // The following property can be used to cause insecure HTTP connections to
  // be redirected to HTTPS.
  requireHttps: true,
  ```

- Finally, to require authentication when accessing HTTP end-points published by [Node-RED][], including pages created using [node-red-dashboard][]:

  ```
  // To password protect the node-defined HTTP endpoints (httpNodeRoot), or
  // the static content (httpStatic), the following properties can be used.
  // The pass field is a bcrypt hash of the password.
  // See http://nodered.org/docs/security.html#generating-the-password-hash
  httpNodeAuth: {user: "...user name...",pass: "...hashed password..."},
  ```

See <http://nodered.org/docs/security.html> for details. Note in
particular the section on generating a hashed a password using command
line tools supplied along with Node-RED.

The same caveats apply here as noted for [mosquitto][permissions] in
regards to file permissions for the *letsencrypt* live certificate
files.

[permissions]: {{< relref "#permissions" >}}
[Node-RED]: https://nodered.org/
[raspbian]: https://www.raspbian.org/
[mosquitto-page]: {{< relref "docs/rpi-config/mosquitto/_index.md" >}}
[node.js]: https://nodejs.org/en/
[debian]: https://www.debian.org/
[node-red-dashboard]: https://flows.nodered.org/node/node-red-dashboard
