---
title: "Apache"
date: 2019-09-11T06:19:07-07:00
anchor: "apache"
weight: 30
---

# Apache

Install [Apache][] server.

## Script

[config-apache2.sh][]

{{< code file="/static/rpi-config/config-apache2.sh" >}}

## Usage

```bash
./config-apache2.sh
```

## Additional Information

Configure your router to forward ports 80 and 443 to your Pi and then
proceed immediately to configure secure access using
[certbot][certbot-page].

[config-apache2.sh]: /rpi-config/config-apache2.sh
[Apache]: https://httpd.apache.org
[certbot-page]: {{< relref "docs/rpi-config/apache/certbot.md" >}}
