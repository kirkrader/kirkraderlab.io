---
title: "Certbot"
date: 2019-09-11T06:19:07-07:00
anchor: "certbot"
weight: 40
---

# Certbot

Install [certbot][] certificate into Apache web server.

## Script

[config-certbot.sh][]

{{< code file="/static/rpi-config/config-certbot.sh" >}}

## Usage

        ./config-certbot.sh

[config-certbot.sh]: /rpi-config/config-certbot.sh
[certbot]: https://certbot.eff.org/
