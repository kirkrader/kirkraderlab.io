---
title: "Samba"
date: 2019-09-11T06:19:07-07:00
anchor: "samba"
weight: 80
---

# Samba

Install and configure [samba][] file server.

## Script

[config-samba.sh][]

{{< code file="/static/rpi-config/config-samba.sh" >}}

## Usage

```bash
./config-samba.sh -u USER -p PASSWORD
```

## Additional Information

This script will install [samba][] and then create a directory named
*/shared* that will be made accessible on your local network,
protected by the given user name and password.

[config-samba.sh]: /rpi-config/config-samba.sh
[samba]: https://www.samba.org
