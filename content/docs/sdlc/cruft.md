---
title: "Cruft-Free Quotient"
weight: 1
# bookFlatSection: false
# bookShowToC: true
---

# Cruft-Free Quotient

## Definition

Let the *cruft-free quotient* be

{{< katex >}}
\mathbb{C}_f = { 1 \over { F + P_i + P_c } }
{{< /katex >}}

where

{{< katex >}}
\begin{aligned}
F   & = \text{the number of third-party build-time dependencies} \\
P_i & = \text{the number of IDE plugins in use} \\
P_c & = \text{the number of CI plugins in use}
\end{aligned}
{{< /katex >}}

The lower the value, the lower the quality of your code
while noting that

{{< katex >}}
{ \lim_{n \to 0} { 1 \over n } } = \infty
{{< /katex >}}

(as embodied, for example, by the [IEEE
754](https://en.wikipedia.org/wiki/IEEE_754#Exception_handling) specification
for floating-point arithmetic in digital computation and since the preceding is
simply the inverse of

{{< katex >}}
{ \lim_{n \to \infty} { 1 \over n } } = 0
{{< /katex >}}

## Axiom

The rationale behind the definition of the *cruft-free quotient* is
simple: frameworks and plugins that "magically" alter code are only
safe to use by highly experienced developers for whom they offer the
least benefit and to whom they simply represent bloat and impediments
to debugging by decoupling behavior from representation in the
software's source code. This includes wildly popular frameworks like
[Spring](http://spring.io).
