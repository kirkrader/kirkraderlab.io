---
title: "Software Development Life-Cycle"
date: 2019-09-15T12:11:57-07:00
weight: 10
---

# Software Development Life-Cycle

Forget Scrum, [SAFe][] or any of their ilk. *Agile* vs. *waterfall* is
a false dichotomy promulgated by consultants in the business of
["selling agile by the pound"][agile-is-dead] [^1].

Here is how enterprise-scale software is and always has been created
and maintained:

{{< mermaid >}}
graph TB
    start(( ))
    inception[Inception]
    execution[Execution]
    verification[Verification]
    operation[Operation]
    start-- request feature -->inception
    inception-- reject -->start
    inception-- analyze -->inception
    inception-- approve -->execution
    execution-- design / implement -->execution
    execution-- request change -->inception
    execution-- release -->verification
    verification-- report bug -->execution
    verification-- test -->verification
    verification-- request change -->inception
    verification-- accept -->operation
    operation-- monitor -->operation
    operation-- report bug -->execution
    operation-- request change -->inception
    operation-- anayltics -->inception
{{< /mermaid >}}

I.e. in the real world there is never a single, linear "waterfall" of
phases leading from inception to delivery nor is there a simple loop
of stand-alone "iterations" each of which comprises all of the
activities necessary to the software development life-cycle compressed
into a short, fixed-length "sprint." As with so much of life, reality
is far more complex, requiring one to think more deeply than simple
slogans or sports analogies.

For software development, this reality is a fairly dense web of
distinct yet interdependent activities. Each such activity proceeds
iteratively but at no fixed "cadence" and more or less asynchronously
from any of the others. Whatever you say about your preferred
"methodology," the preceding diagram depicts what you are actually
doing if you have achieved any degree of long-term success in
developing, deploying and maintaining big, complex software
systems. Despite what fadists and hucksters might claim, this has not
changed substantially since the 1980's when the author began his
career in the software engineering industry.

Most importantly, each transition / dependency in the preceding graph
must be accompanied by appropriate artifacts. Obviously, what gets
delivered as part of a release includes the "working software" that is
the focus of the second statement in the [Manifesto for Agile Software
Development][manifesto] but it had better also include release notes,
installation and configuration guides, user manuals etc. or the
software won't actually ever get working or stay working in production
for long. Prior to that, when a new or changed feature is approved,
that approval is useless unless it is accompanied by sufficient
documentation describing the intended new behavior and its impacts on
other new or existing features that may also have to change as a
result. Similarly, the teams responsible for operating long-lived
software systems are doing themselves and the organizations to which
they belong no favors by relying on "tribal knowledge" rather than
carefully crafted and maintained run-books and the like. Ditto for
test procedures carried out during software verification and
acceptance.

[^1]: Dave Thomas, co-author and signatory of the
      *Manifesto for Agile Software Development*

[safe]: https://scaledagileframework.com/
[agile-is-dead]: https://youtu.be/a-BOSpxYJ9M
[manifesto]: https://agilemanifesto.org/
