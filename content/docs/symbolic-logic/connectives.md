---
title: "Connectives"
weight: 2
# bookFlatSection: false
# bookShowToC: true
---

# Connectives

## Natural vs. Formal Language

Many students find the formal definition for some of these
connectives, especially the [condition connective](#conditional),
counter-intuitive, thinking that one or more rows of some truth tables
should be marked as "undetermined" or some such. This reflects one of
the central issues with trying to equate formulas of symbolic logic
with natural language expressions already alluded to in discussing the
[sentential calculus][]. One must simply accept that within the domain
of discourse of the sentential and monadic predicate calculi every
well-formed formula has a truth value and their respective truth
tables define the semantics of the connectives. Otherwise, one might
as well stop reading this document or any other work on "traditional"
logic.

That said, the best way to "feel" the correctness of the truth table
for the conditional connective, &Phi; &rightarrow; &Psi;, is by
understanding it to mean that there are no possible circumstances
under which &Phi; can be true while &Psi; is false or, more
succinctly, that &Phi; implies &Psi;. Any trial lawyer will tell you
that the burden of proof for such inferences in the real world is
almost always close to impossibly high, which is part of the reason
for the uneasiness with which this truth table is inevitably first
met.

> In US courts, as in most legal systems based on British Common Law,
> the burden of proof for finding a defendant guilty of a crime is
> "beyond a reasonable doubt" which is a lower standard of proof than
> "beyond any possibility of doubt." The standard is even lower in
> civil litigation where the burden is simply that of a "preponderance
> of the evidence." These lesser standards for proving legal liability
> reflect the degree to which it is simply a mistake to believe that
> symbolic logic has much utility in settling _a posteriori_ arguments
> regarding contingent facts, as discussed further in [Validity][].

## Overview

The connectives of the sentential calculus are formally defined using
*truth tables*.

- Each column of a truth table represents one term of a well-formed
  formula, i.e. one node in a [syntax tree][] like that shown
  elsewhere.

- Each row represents one possible combination of truth values of the
  primitive sentences -- i.e. terminal nodes -- that appear anywhere
  in the formula.

For each column that represents a formula joined by connectives, the
truth value for that column is calculated according to the truth table
of that connective and the values for all its terms in the same row.

<a id="negation"></a>

## Negation ("Not")

Negation inverts the truth value of the term to which it applies:

{{< katex >}}
\begin{array}{c|c}
\Phi & \neg \Phi \\
\hline           \\
T    & F         \\
F    & T
\end{array}
{{< /katex >}}

<a id="conjunction"></a>

## Conjunction ("And")

Conjunction is true if and only if both its terms are true:

{{< katex >}}
\begin{array}{cc|c}
\Phi & \Psi & \Phi \wedge \Psi \\
hline                          \\
T    & T    & T                \\
T    & F    & F                \\
F    & T    & F                \\
F    & F    & F
\end{array}
{{< /katex >}}

<a id="disjunction"></a>

## Disjunction ("Inclusive Or")

Disjunction is true if either or both of its terms are true:

{{< katex >}}
\begin{array}{cc|c}
\Phi & \Psi & \Phi \vee \Psi \\
\hline                       \\
T    & T    & T              \\
T    & F    & T              \\
F    & T    & T              \\
F    & F    & F
\end{array}
{{< /katex >}}

<a id="conditional"></a>

## Conditional ("If")

A conditional formula is false if and only if its *antecedent* (the
term to the left of the arrow) is true and its *consequent* (the term
to the right of the arrow) is false.

{{< katex >}}
\begin{array}{cc|c}
\Phi & \Psi & \Phi \rightarrow \Psi \\
\hline                              \\
T    & T    & T                     \\
T    & F    & F                     \\
F    & T    & T                     \\
F    & F    & T
\end{array}
{{< /katex >}}

<a id="biconditional"></a>

## Biconditional ("If and only if")

A biconditional is true when both its terms have the same truth value,
false otherwise. For this reason, it is sometimes described as meaning
"equivalence."

{{< katex >}}
\begin{array}{cc|c}
\Phi & \Psi & \Phi \leftrightarrow \Psi \\
\hline                                  \\
T    & T    & T                         \\
T    & F    & F                         \\
F    & T    & F                         \\
F    & F    & T
\end{array}
{{< /katex >}}

The double-headed arrow symbol (&harr;) is used due to the fact that
*P* &harr; *Q* is equivalent to:

{{< katex >}}
\left( P \rightarrow Q \right) \wedge \left( Q \rightarrow P \right)
{{< /katex >}}

Or, in English, "*P* if and only if *Q*" is the same as "*P* implies
*Q* and *Q* implies *P*."

[sentential calculus]: {{< relref "sentential-calculus" >}}
[validity]: {{< relref "validity" >}}
[syntax tree]: {{< relref "sentential-calculus#syntax-tree" >}}
