---
title: "Backus-Naur Form"
weight: 8
# bookFlatSection: false
# bookShowToC: true
---

# Backus-Naur Form

Here is the formal syntax definition for well-formed formulas (WFF's) of the
sentential and monadic predicate calculi using <a
href="https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_Form">Extended
Backus-Naur Form</a> (EBNF) \cite EBNF notation for context-free grammars. In
each production rule, symbols enclosed in apostrophes are literal strings and
vertical bars separate alternative sequences. Ellipses are used to denote sets
of all possible alphabetic characters in a given case. Parentheses are used for
grouping sequences.

## EBNF

wff ::= term '.';

term ::= sentence | predicate | generalization | negation |
         connective | description | identity;

sentence ::= upper-case;

predicate ::= upper-case lower-case;

generalization ::= quantifier lower-case term;

negation ::= '&not;' term;

connective ::= '(' term operator term ')';

description ::= '&iota;' lower-case term;

identity ::= lower-case '=' lower-case;

operator ::= and | or | if | iff

quantifier ::= universal | existential;

and ::= '&wedge;';

or ::= '&vee;';

if ::= '&rightarrow;';

iff ::= '&harr;';

universal ::= '&forall;';

existential ::= '&exist;';

upper-case ::= 'A' ... 'Z';

lower-case ::= 'a' ... 'z';

The preceding defines the "official" syntax that is amenable, for
example, to automatic parsing as by a computer algorithm. The examples
throughout this document use a more relaxed syntax, of the sort that
human beings are likely to write on whiteboards and the like. For
example, the following are perfectly understandable by human readers
but do *not* conform to the preceding production rules due to missing
parentheses and periods:

- *P*

- *P* &vee; *Q*

- &forall;*xy*(*Fx* &rightarrow; *Gy*)

Here are the corresponding "proper" wff's:

- *P*.

- (*P* &vee; *Q*).

- &forall;*x*(&forall;*y*(*Fx* &rightarrow; *Gy*)).
