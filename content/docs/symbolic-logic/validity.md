---
title: "Validity"
weight: 3
# bookFlatSection: false
# bookShowToC: true
---

# Validity

## *A Priori* vs. *A Posteriori* Truth

An argument is *valid* if its conclusion is necessarily true on the
assumption that all of its premises are true without regard to whether
or not those premises actually are true in the real world. As
described [Sentential Calculus][], a sentence denoted by a primitive
term in the sentential calculus may be either true or false. If *P* is
defined to correspond to some contingent fact about the world that can
only be determined by empirical observation -- "It is raining," "The
moon is full" etc. -- its truth value is said to be known *a
posteriori* (i.e. "after observation") and symbolic logic by itself
says nothing about its actual truth value at any given point in
spacetime.

A "fact" that can be known with certainty without knowing anything
about the current state of affairs is said to be an *a priori* ("prior
to observation") truth. An argument does not need to be a premise-less
[tautology][tautologies] in order to be valid. The following argument
is valid. Whether or not its conclusion is actually true depends on
the current of the world in regards to its premises:

{{< katex >}}
\begin{aligned}
&P \rightarrow Q \\
&P               \\
\hline           \\
\therefore &Q
\end{aligned}
{{< /katex >}}

> Symbolic logic is useful in providing tools for verifying the
> validity of some piece of *a priori* reasoning while not even
> attempting to verify any bit of *a posteriori* truth.

[sentential calculus]: {{< relref "sentential-calculus" >}}
[tautologies]: {{< relref "tautologies" >}}
