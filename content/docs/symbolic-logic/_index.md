---
title: "Symbolic Logic"
weight: 20
# bookFlatSection: false
# bookShowToC: true
---

# Symbolic Logic

> This section began as an informal tutorial on the monadic predicate
> calculus presented to a group of co-workers, at their request.

> No, really!

- [Sentential Calculus]({{< relref "sentential-calculus" >}})

- [Connectives]({{< relref "connectives" >}})

- [Validity]({{< relref "validity" >}})

- [Inference Rules]({{< relref "inference-rules" >}})

- [Tautologies]({{< relref "tautologies" >}})

- [Proofs]({{< relref "proofs" >}})

- [Monadic Predicate Calculus]({{< relref "monadic-predicate-calculus" >}})

- [Backus-Naur Form]({{< relref "bnf" >}})
