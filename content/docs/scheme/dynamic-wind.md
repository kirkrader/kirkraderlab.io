---
title: "dynamic-wind"
weight: 80
# bookFlatSection: false
# bookShowToC: true
---

# dynamic-wind

The built-in `dynamic-wind` procedure is Scheme's equivalent to Common
Lisp's `unwind-protect`, the `try ... finally ...` construct in
languages like C# and Java and so on.

The main entry point for this example is a provedure named
`drill-hole`. It uses the simulated [drill press][] described
earlier. The `drill-hole` function defines several private functions
including `start-drilling`, `keep-drilling` and `stop-drilling` that
are used in the arguments passed to `dynamic-wind` in the body of
`drill-hole`.  As noted in the chapter on [lexical closures][] where
[make-drill][] was defined, the simulated drill press has a number of
run-time constraints such as that the drill motor can only be started
or stopped when the drill height is 0. The `drill-hole` procedure is a
wrapper for an instance of such a simulated drill that uses
`dynamic-wind` to protect the wrapped drill from violations of such
constraints even when its `keep-drilling` loop is exited and
re-entered using a [continuation][call-with-current-continuation].

The result of calling `keep-drilling` is bound to the local variable
`k` in the body of `drill-hole`. That call to `keep-drilling` is
protected during stack winding and unwinding by calls to
`start-drilling` and `stop-drilling`, respectively, by passing all
three functions in a call to `dynamic-wind`.

Those familiar with languages with constructs like `(unwind-protect
...)` or `try ... finally ...` can understand the relationship between
`keep-drilling` and `stop-drilling` as being an enhanced version of
such constructs.

In other words, `dynamic-wind` guarantees to call `stop-drilling`
after `keep-drilling` exits, whether or not the latter exits normally
or through some non-sequential flow of control, as in the following
Common Lisp code:

```lisp
(unwind-protect

  (progn
     (start-drilling)
     (keep-drilling))

  (stop-drilling))
```

Or in the case of languages like Java and C#, something like:

```java
try {

    startDrilling();
    keepDrilling();

} finally {

    stopDrilling();

}
```

In C++, similar stack-unwinding protections are achieved using
"automatic" variables of types with destructors.

The additional functionality offered by `dynamic-wind` over the
`unwind-protect` and `try ... finally ...` examples is that
`dynamic-wind` also guarantees that `start-drilling` will be called
before `keep-drilling` starts execution the first time it is entered
*and* if it is re-entered by invoking a continuation after it has
previously exited.

This stack winding / unwinding / rewinding behavior is demonstrated in
the body of `drill-hole`. In particular, `keep-drilling` bores the
hole 50% through the full range of the possible bit positions and then
exits, returning a continuation that is later used to resume drilling
until the hole is 100% complete. Looking at the [output](#output),
one can see that `start-drilling` and `stop-drilling` are called the
appropriate number of times (twice each), in the appropriate places in
the overall flow of control (each time the body of `keep-drilling` is
about to be entered and after each time `keep-drilling` exits,
respectively).

<a id="drill-hole"></a>

Here is the Scheme source for `drill-hole`:

{{< code file="/docs/scheme/drill-hole.scm" language="lisp" >}}

(download [drill-hole.scm][])

<a id="output"></a>

Here is the output of [(drill-hole)](#drill-hole):

```
start-drilling called
started motor
keep-drilling entered
stepping bit position from 0 to 50 by 1
hole 50% drilled, pausing to let bit cool
stop-drilling called
stepping bit position from 50 to 0 by -1
motor stopped
waiting a bit (pun intended)
start-drilling called
started motor
keep-drilling re-entered
continuing to drill
stepping bit position from 0 to 100 by 1
hole completely drilled
stop-drilling called
stepping bit position from 100 to 0 by -1
motor stopped
```

[drill press]: {{< relref "lexical-closures#make-drill" >}}
[call-with-current-continuation]: {{< relref "call-with-current-continuation" >}}
[drill-hole.scm]: /docs/scheme/drill-hole.scm
