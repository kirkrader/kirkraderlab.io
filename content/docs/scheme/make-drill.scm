;; -*- geiser-scheme-implementation: guile -*-

;; Copyright 2016 Kirk Rader
;;
;; Licensed under the Apache License, Version 2.0 (the "License"); you
;; may not use this file except in compliance with the License.  You
;; may obtain a copy of the License at
;;
;; http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
;; implied.  See the License for the specific language governing
;; permissions and limitations under the License.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Function: (make-drill)
;;
;; Create a simulated drill press.
;;
;; Usage: (let ((drill (make-drill)))
;;           (drill 'start-motor!)
;;           (drill 'move-bit! 50)
;;           (drill 'move-bit! 0)
;;           (drill 'stop-motor!))
;;
;; A drill has two attributes:
;;
;;   motor-state: 'stopped or 'running (initially 'stopped)
;;
;;   bit-position: integer between 0 and 100 (initially 0)
;;
;; A bit position of 0 indicates that the bit is in its highest
;; position while 100 indicates that the bit is in its lowest
;; position.
;;
;; It supports the following messages:
;;
;;   'get-motor-state - returns current motor-state
;;
;;   'get-bit-position - returns current bit-position
;;
;;   'move-bit! new-position - simulates a stepper motor controlling
;;                             bit height
;;
;;   'start-motor! - set motor-state to 'running
;;
;;   'stop-motor! - set motor-state to 'stopped
;;
;; the message handlers collectively perform checks to enforce the
;; following constraints:
;;
;;   * the bit position must be an integer between 0 and 100 (inclusive)
;;
;;   * the motor can only change state when the bit position is 0
;;
;;   * the bit can be greater than 0 only when the motor is running
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define (make-drill)

  (let ((motor-state
         ;; state of the main motor
         'stopped)

        (bit-position
         ;; value determined by a stepper motor
         ;; controlling the height of the bit
         0))

    ;; A drill is a function that takes a message and additional
    ;; arguments that depend on the particular message
    (lambda (message . arguments)

      (letrec ((signum
                ;; return 0, 1 or -1 based on the sign of the given
                ;; number
                (lambda (x)
                  (cond ((negative? x) -1)
                        ((positive? x) 1)
                        (else 0))))

               (set-bit-position!
                ;; instruct the stepper motor to move the bit to the
                ;; specified height
                (lambda (new-position increment)
                  (display "stepping bit position from ")
                  (display bit-position)
                  (display " to ")
                  (display new-position)
                  (display " by ")
                  (display increment)
                  (newline)
                  (let loop ((done (= bit-position new-position)))
                    (unless done
                      (set! bit-position (+ bit-position increment))
                      (loop (= bit-position new-position))))))

               (move-bit!
                ;; set the value of bit-position or throw an exception
                ;; if constraints would be violated
                ;;
                ;; see set-bit-position!
                (lambda (new-position)
                  (cond ((not
                          (and (integer? new-position)
                               (>= new-position 0)
                               (<= new-position 100)))
                         (throw 'drill "invalid bit position" new-position))
                        ((and (eq? motor-state 'stopped)
                              (> new-position 0))
                         (throw 'drill "motor not running" new-position))
                        (else
                         (set-bit-position!
                          new-position
                          (signum (- new-position bit-position)))))))

               (start-motor!
                ;; Set the value of motor-state to 'running or throw
                ;; an exception if constraints would be violated
                (lambda ()
                  (cond ((eq? motor-state 'running)
                         (throw 'drill "motor already running"))
                        ((not (= bit-position 0))
                         (throw 'drill "bit position not zero" bit-position))
                        (else
                         (display "started motor")
                         (newline)
                         (set! motor-state 'running)))))

               (stop-motor!
                ;; Set the value of motor-state to 'stopped or throw
                ;; an exception if constraints would be violated
                (lambda ()
                  (cond ((eq? motor-state 'stopped)
                         (throw 'drill "motor not running"))
                        ((not (= bit-position 0))
                         (throw 'drill "bit position not zero" bit-position))
                        (else
                         (display "motor stopped")
                         (newline)
                         (set! motor-state 'stopped))))))

        (case message

          ((get-bit-position)
           (unless (null? arguments)
             (throw 'drill "invalid-arguments" (cons message arguments)))
           bit-position)

          ((get-motor-state)
           (unless (null? arguments)
             (throw 'drill "invalid arguments" (cons message arguments)))
           motor-state)

          ((move-bit!)
           (unless (= 1 (length arguments))
             (throw 'drill "invalid arguments" (cons message arguments)))
           (move-bit! (car arguments)))

          ((start-motor!)
           (unless (null? arguments)
             (throw 'drill "invalid arguments" (cons message arguments)))
           (start-motor!))

          ((stop-motor!)
           (unless (null? arguments)
             (throw 'drill "invalid arguments" (cons message arguments)))
           (stop-motor!))

          (else
           (throw 'drill "unsupported message" (cons message arguments))))))))
