---
title: "Scheme"
date: 2019-09-16T16:05:54-07:00
weight: 30
---

# Scheme

> As with the section on [symbolic logic][], a few gluttons for punishment
> among my colleagues asked for this.
>
> Ok, one day my boss asked "what's all this I hear about Scheme continuations,"
> (more or less) and the discussion eventually evolved into this.

Examples demonstrating various Scheme features and built-in procedures
including:

- Tail recursion

- Lexical closures

- `call-with-current-continuation -- a.k.a. `call/cc`

- `dynamic-wind`

They provide simple, if somewhat contrived, samples of idioms commonly used in
Scheme_ programming.

\note These examples were created and debugged using the Guile --
http://www.gnu.org/software/guile/ -- dialect, but should work as-is in most
variants of Scheme.

- [Basics][]

- [Tail Recursion][]

- [Lexical Closures][]

- [call-with-current-continuation][]

- [dynamic-wind][]

- [engines][]

[symbolic logic]: {{< relref "/docs/symbolic-logic" >}}
[basics]: {{< relref "basics" >}}
[tail recursion]: {{< relref "tail-recursion" >}}
[lexical closures]: {{< relref "lexical-closures" >}}
[call-with-current-continuation]: {{< relref "call-with-current-continuation" >}}
[dynamic-wind]: {{< relref "dynamic-wind" >}}
[engines]: {{< relref "engines" >}}
