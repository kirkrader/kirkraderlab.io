---
title: "Hubitat and MQTT"
weight: 3
# bookFlatSection: false
# bookShowToC: true
---

# Hubitat and MQTT

{{< mermaid >}}
graph TB

    subgraph Home Network

        hubitat[Hubitat Elevation]
        bridge[Hue Bridge]
        hue[Hue Devices]
        zwave[Z-Wave Devices]
        more[more devices<br>and services]

        subgraph Raspberry Pi
            mosquitto[Mosquitto]
            nodered[Node-RED]
            zstick[Aeotec Z-Stick]
        end

        hubitat == "MQTT" === mosquitto
        nodered == "Hue API" === bridge
        bridge -. Zigbee ..- hue
        zstick -. Z-Wave ..- zwave
        hubitat -.- more
        nodered == OpenZWave === zstick
        nodered == MQTT === mosquitto

    end

    classDef feature fill:#ffaaff,stroke:#000000,stroke-width:1px
    class hubitat,mosquitto feature
{{< /mermaid >}}

While I use [Node-RED][] as my primary home automation engine, there
are certain devices and services which it does not support. I use a
[Hubitat Elevation][hubitat] hub for integrating such devices and
services into my [Node-RED][] flows. The reason is simple: while
[Hubitat][] does provide native support for a number of devices and
services not supported directly by [Node-RED][] it is quite deficient
in most other ways. Its built-in _Rule Machine_ app is missing a
number of critical features and has an crude user interface that makes
it extremely painful to use. Ditto for its built-in dashboarding
features.

Among the deficiencies that prevent me from relying on [Hubitat][] as
my primary home automation hub is its failure to support MQTT as a
feature in its _Rule Machine_ app. The only support for the MQTT
protocol is hidden deep in the bowels of its custom driver API. While
it is possible to use [Hubitat's][hubitat] _Maker API_ app to
integrate with [Node-RED][] via a web service, like most of
[Hubitat's][hubitat] features the _Maker API_ app is inefficiently
implemented and burdensome to use. I far prefer the lighter weight,
better optimized MQTT protocol for home automation applications.

As a learning experience and to provide reliable MQTT support within
my personal home automation environment I created my own simple
[hubitat-mqtt-connection][] driver. This is _not_ a competitor to the
excellent _MQTT Client_ app under development by [Kevin
H][hubitat-community]. Most people will be far happier using his app
than attempting to use device drivers. I make mine available publicly
simply for tutorial purposes and possibly as an inspiration for your
own efforts.

- [mqtt-connection-driver.groovy][]
- [mqtt-handler-driver.groovy][]

Together, the preceding two drivers support:

1. Listening for MQTT messages on particular topics as _Rule Machine_
   triggers
2. Accessing the payloads of MQTT messages in _Rule Machine_ triggers
   and actions
3. Publishing MQTT messages on any topic with any payload and values
   for _qos_ and _retain_ in _Rule Machine_ actions

[mqtt-connection-driver.groovy]: https://raw.githubusercontent.com/parasaurolophus/hubitat-mqtt-connection/master/mqtt-connection-driver.groovy
[mqtt-handler-driver.groovy]: https://raw.githubusercontent.com/parasaurolophus/hubitat-mqtt-connection/master/mqtt-handler-driver.groovy
[hubitat]: https://www.hubitat.com
[node-red]: https://gitlab.com/parasaurolophus/cheznous
[zstick]: https://aeotec.com/z-wave-usb-stick/
[node-red-contrib-openzwave-am]: https://flows.nodered.org/node/node-red-contrib-openzwave-am
[hubitat-community]: https://community.hubitat.com/t/mqtt-client/3808/34
[hubitat-mqtt-connection]: https://github.com/parasaurolophus/hubitat-mqtt-connection
