#!/bin/bash

usage() {

  if [[ -n "$2" ]]; then
    echo "$2" 1>&2
  fi

  echo "Usage: $0 -d domain -t token" 1>&2
  exit $1

}

while getopts "hd:t:" current; do
  case "${current}" in
    d)
      DOMAIN="${OPTARG}"
      ;;
    t)
      TOKEN="${OPTARG}"
      ;;
    h)
      usage 0
      ;;
    :)
      usage 3 "Missing value for -${OPTARG}"
      ;;
    *)
      usage 4
      ;;
  esac
done

if [[ -z "${DOMAIN}" ]]; then
  usage 1 "duckdns.org domain name required"
fi

if [[ -z "${TOKEN}" ]]; then
  usage 2 "duckdns.org token required"
fi


mkdir -p ~/duckdns
cd ~/duckdns

cat <<EOF > duck.sh
echo url="https://www.duckdns.org/update?domains=${DOMAIN}&token=${TOKEN}&ip=" | curl -k -o ~/duckdns/duck.log -K -
EOF

chmod 700 duck.sh

if [[ $(crontab  -l) = "no crontab for ${USER}" ]]; then
  echo "*/5 * * * * ~/duckdns/duck.sh >/dev/null 2>&1" | crontab -
else
  ( crontab -l ; echo "*/5 * * * * ~/duckdns/duck.sh >/dev/null 2>&1" ) | crontab -
fi
