#!/bin/bash

sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y install emacs elpa-markdown-mode pandoc

mkdir -p ~/.emacs.d

if [[ ! -f ~/emacs.d/init.el ]]; then

cat <<EOF > ~/.emacs.d/init.el
(package-initialize)
(setq-default indent-tabs-mode nil)
(setq make-backup-files nil)
(setq markdown-command "pandoc")
EOF

fi
