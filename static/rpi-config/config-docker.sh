#!/bin/bash

sudo apt-get update && apt-get upgrade -y
curl -fsSL https://get.docker.com | sh
sudo usermod -aG docker ${USER}
# sudo shutdown -r now
