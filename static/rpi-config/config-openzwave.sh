#!/bin/bash

sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y install build-essential git libudev-dev
git clone https://github.com/OpenZWave/open-zwave.git
cd open-zwave
git checkout v1.6
sed -i -e 's/-Werror//g' cpp/build/Makefile
sed -i -e 's/-Werror//g' cpp/hidapi/configure.ac
make
sudo make install
sudo ldconfig /usr/local/lib/
