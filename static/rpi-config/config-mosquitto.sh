#!/bin/bash

# This assumes Raspbian buster

# For stretch you may have to configure apt-get
# to use the mosquitto repository and key

sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y install mosquitto mosquitto-clients
