#!/bin/bash

# install minidlna

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y minidlna

# create the media directories

sudo mkdir -p -m 1777 /shared
sudo mkdir -p -m 1777 /shared/Music
sudo mkdir -p -m 1777 /shared/Pictures
sudo mkdir -p -m 1777 /shared/Video

# update /etc/minidlna.conf

sudo sed -i 's+#user=minidlna+user=minidlna+' /etc/minidlna.conf
sudo sed -i 's+media_dir=/var/lib/minia+#media_dir=/var/lib/minidlna+' /etc/minidlna.conf

cat <<EOF | sudo tee -a /etc/minidlna.conf

media_dir=A,/shared/Music
media_dir=V,/shared/Video
media_dir=P,/shared/Pictures
EOF

# restart the minidlna service so that configuration changes will take effect

sudo systemctl restart minidlna

# now start copying media files into the shared directories
