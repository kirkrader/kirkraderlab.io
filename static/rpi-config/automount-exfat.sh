#!/bin/bash

# change the UUID to match your actual disk drive as reported by
# blkid, e.g.

# $ blkid
# /dev/sda1: LABEL="boot" UUID="DDAB-3A15" TYPE="vfat" PARTUUID="fb2bc19d-01"
# /dev/sda2: LABEL="rootfs" UUID="5fa1ec37-3719-4b25-be14-1f7d29135a13" TYPE="ext4" PARTUUID="fb2bc19d-02"
# /dev/sdb1: UUID="5EA9-AFBB" TYPE="exfat" PTTYPE="dos" PARTLABEL="Microsoft basic data" PARTUUID="6b400c2e-5e2c-48b8-a05e-61ff41e48a66"

# meaning that UUID is 5EA9-AFBB

# so to mount drive 5EA9-AFBB at /data each time the Pi boots:

#     ./rpi-automation-exfat -d /data -u 5EA9-AFBB

usage() {

  if [[ -n "$2" ]]; then
    echo "$2" 1>&2
  fi

  echo "Usage: $0 -d mountpoint -u uuid" 1>&2
  exit $1

}

while getopts 'hd:u:' current; do
    case "${current}" in
	h)
	    usage 0
	    ;;
	d)
	    DIRECTORY="${OPTARG}"
	    ;;
	u)
	    UUID="${OPTARG}"
	    ;;
	:)
	    usage 3 "Missing value for -${OPTARG}"
	    ;;
	*)
	    usage 4
	    ;;
    esac
done

if [[ -z "${DIRECTORY}" ]]; then
    usage 1 "Missing -d parameter"
fi

if [[ -z "${UUID}" ]]; then
    usage 2 "Missing -u parameter"
fi

# create the mount point

sudo mkdir -p "${DIRECTORY}"

# add your drive to /etc/fstab so that it will automatically mount

cat <<EOF | sudo tee -a /etc/fstab
UUID=${UUID} ${DIRECTORY} exfat defaults,auto,umask=000,users,rw 0 0
EOF

# sudo shutdown -r now
