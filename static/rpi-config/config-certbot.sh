#!/bin/bash

# Note: assumes Debian 10 (buster)

# For Debian 9 (stretch) you will need
# to configure the backports repository
# for apt-get in order to install the
# certbot package

sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get install certbot python-certbot-apache
sudo certbot --apache
