#!/bin/bash

usage() {

  if [[ -n "$2" ]]; then
    echo "$2"
  fi

  echo "Usage: $0 -u user -p password"
  exit $1
}

while getopts "hu:p:" current; do
  case "${current}" in
    h)
      usage 0
      ;;
    u)
      USERNAME="${OPTARG}"
      ;;
    p)
      PASSWORD="${OPTARG}"
      ;;
    :)
      usage 3 "Missing value for -${OPTARG}"
      ;;
    *)
      usage 4
      ;;
  esac
done

if [[ -z "${USERNAME}" ]]; then
  usage 1 "samba user name required"
fi

if [[ -z "${PASSWORD}" ]]; then
  usage 2 "samba password required"
fi

# install samba

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y samba samba-common-bin

# create the shared directory

sudo mkdir -p -m 1777 /shared

# set the samba password for the specified user

# note that the samba password does not need to match your raspbian credentials
# and for security reasons should not

sudo smbpasswd -s -a "${USERNAME}" <<EOF
${PASSWORD}
${PASSWORD}
EOF

# add the shared directory to /etc/samba/smb.conf

# change Guest ok = no to Guest ok = yes if you want to support anonymous access

cat <<EOF | sudo tee -a /etc/samba/smb.conf

[shared]
Comment = Pi shared folder
Path = /shared
Browseable = yes
Writeable = Yes
only guest = no
create mask = 0777
directory mask = 0777
Public = yes
Guest ok = no
EOF

# restart the samba service so that the configuratin changes will take effect

sudo /etc/init.d/samba restart
